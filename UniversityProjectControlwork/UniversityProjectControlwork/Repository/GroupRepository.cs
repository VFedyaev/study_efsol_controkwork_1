﻿using UniversityProjectControlwork.Models;
using UniversityProjectControlwork.Repository.Contracts;

namespace UniversityProjectControlwork.Repository
{
    public class GroupRepository : Repository<Group>, IGroupRepository
    {
        public GroupRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Groups;
        }
    }
}

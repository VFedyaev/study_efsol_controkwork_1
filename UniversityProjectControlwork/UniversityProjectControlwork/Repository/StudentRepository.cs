﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using UniversityProjectControlwork.Models;
using UniversityProjectControlwork.Repository.Contracts;

namespace UniversityProjectControlwork.Repository
{
    public class StudentRepository : Repository<Student>, IStudentRepository
    {
        public StudentRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Students;
        }

        public IEnumerable<Student> GetStudentsWithGroups()
        {
            return DbSet.Include(g => g.Group).ToList();
        }

        public Student GetStudentsWithGroupsById(int? id)
        {
            return DbSet.Include(g => g.Group).FirstOrDefault(e => e.Id == id);
        }
    }
}

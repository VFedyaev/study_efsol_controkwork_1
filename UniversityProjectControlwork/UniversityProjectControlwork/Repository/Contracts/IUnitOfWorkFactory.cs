﻿using System;

namespace UniversityProjectControlwork.Repository.Contracts
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork MakeUnitOfWork();
    }
}

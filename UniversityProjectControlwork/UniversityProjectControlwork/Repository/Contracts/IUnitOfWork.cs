﻿using System;
using System.Data;
using System.Threading.Tasks;

namespace UniversityProjectControlwork.Repository.Contracts
{
    public interface IUnitOfWork : IDisposable
    {
        IStudentRepository Students { get; }
        IGroupRepository Groups { get; }

        Task<int> CompleteAsync();
        void BeginTransaction();
        void BeginTransaction(IsolationLevel level);
        void RollbackTransaction();
        void CommitTransaction();
    }
}

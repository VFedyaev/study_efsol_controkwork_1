﻿using UniversityProjectControlwork.Models.Base;
using System.Collections.Generic;

namespace UniversityProjectControlwork.Repository.Contracts
{
    public interface IRepository<T> where T : EntityBase
    {
        IEnumerable<T> GetAll();
        T GetById(int? id);
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}

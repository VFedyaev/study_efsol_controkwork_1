﻿using System.Collections.Generic;
using UniversityProjectControlwork.Models;

namespace UniversityProjectControlwork.Repository.Contracts
{
    public interface IStudentRepository : IRepository<Student>
    {
        IEnumerable<Student> GetStudentsWithGroups();
        Student GetStudentsWithGroupsById(int? id);
    }
}

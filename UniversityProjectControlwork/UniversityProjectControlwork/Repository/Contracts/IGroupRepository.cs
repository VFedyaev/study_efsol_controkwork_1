﻿using UniversityProjectControlwork.Models;

namespace UniversityProjectControlwork.Repository.Contracts
{
    public interface IGroupRepository : IRepository<Group>
    {
    }
}

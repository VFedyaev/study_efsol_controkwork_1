﻿using System;

namespace UniversityProjectControlwork.Repository.Contracts
{
    public interface IApplicationDbContextFactory
    {
        ApplicationDbContext Create();
    }
}

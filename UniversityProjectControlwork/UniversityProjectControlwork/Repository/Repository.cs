﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using UniversityProjectControlwork.Models.Base;
using UniversityProjectControlwork.Repository.Contracts;

namespace UniversityProjectControlwork.Repository
{
    public class Repository<T> : IRepository<T> where T : EntityBase
    {
        protected ApplicationDbContext _context;
        protected DbSet<T> DbSet { get; set; }

        public Repository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<T> GetAll()
        {
            return DbSet.ToList();
        }

        public T GetById(int? id)
        {
            return DbSet.FirstOrDefault(e => e.Id == id);
        }

        public void Add(T entity)
        {
            DbSet.Add(entity);
            _context.SaveChanges(); 
        }

        public void Update(T entity)
        {
            _context.Update(entity);
            _context.SaveChanges();
        }

        public void Delete(T entity)
        {
            DbSet.Remove(entity);
            _context.SaveChanges();
        } 
    }
}

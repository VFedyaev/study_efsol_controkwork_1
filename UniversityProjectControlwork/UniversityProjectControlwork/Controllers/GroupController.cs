﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using UniversityProjectControlwork;
using UniversityProjectControlwork.Models;
using UniversityProjectControlwork.Repository.Contracts;

namespace UniversityProjectControlwork.Controllers
{
    public class GroupController : Controller
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public GroupController(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        // GET: Groups
        public IActionResult Index()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var groups = unitOfWork.Groups.GetAll();
                return View(groups);
            }
        }

        // GET: Groups/Details/5
        public IActionResult Details(int? id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (id == null)
                {
                    return NotFound();
                }

                var @group = unitOfWork.Groups.GetById(id);
                if (@group == null)
                {
                    return NotFound();
                }

                return View(@group);
            }
        }

        // GET: Groups/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Groups/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Name,Id")] Group @group)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (ModelState.IsValid)
                {
                    unitOfWork.Groups.Add(@group);
                    return RedirectToAction(nameof(Index));
                }
                return View(@group);
            }          
        }

        // GET: Groups/Edit/5
        public IActionResult Edit(int? id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (id == null)
                {
                    return NotFound();
                }

                var @group = unitOfWork.Groups.GetById(id);
                if (@group == null)
                {
                    return NotFound();
                }
                return View(@group);
            }
        }

        // POST: Groups/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("Name,Id")] Group @group)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (id != @group.Id)
                {
                    return NotFound();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        unitOfWork.Groups.Update(@group);
                    }
                    catch (DbUpdateConcurrencyException ex)
                    {
                        throw ex;
                    }
                    return RedirectToAction(nameof(Index));
                }
                return View(@group);
            }
        }

        // GET: Groups/Delete/5
        public IActionResult Delete(int? id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (id == null)
                {
                    return NotFound();
                }

                var @group = unitOfWork.Groups.GetById(id);
                if (@group == null)
                {
                    return NotFound();
                }

                return View(@group);
            }    
        }

        // POST: Groups/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var @group = unitOfWork.Groups.GetById(id);
                unitOfWork.Groups.Delete(@group);
                return RedirectToAction(nameof(Index));
            } 
        }
    }
}

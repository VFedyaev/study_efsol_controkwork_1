﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using UniversityProjectControlwork.Models;
using UniversityProjectControlwork.Repository.Contracts;

namespace UniversityProjectControlwork.Controllers
{
    public class StudentController : Controller
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public StudentController(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        //GET: Student
        public IActionResult Index()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var students = unitOfWork.Students.GetStudentsWithGroups();
                return View(students);
            }
        }

        //GET: Student/Details/5
        public IActionResult Details(int? id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (id == null)
                {
                    return NotFound();
                }

                var student = unitOfWork.Students.GetStudentsWithGroupsById(id);
                if (student == null)
                {
                    return NotFound();
                }

                return View(student);
            }
        }

        // GET: Student/Create
        public IActionResult Create()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                ViewData["GroupName"] = new SelectList(unitOfWork.Groups.GetAll(), "Id", "Name");
                return View();
            }
        }

        // POST: Student/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("FullName,DateBorn,Address,Gradebook_Number,GroupId,Id")] Student student)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (ModelState.IsValid)
                {
                    unitOfWork.Students.Add(student);
                    return RedirectToAction(nameof(Index));
                }
                ViewData["GroupName"] = new SelectList(unitOfWork.Groups.GetAll(), "Id", "Name", student.GroupId);
                return View(student);
            }
        }

        // GET: Student/Edit/5
        public IActionResult Edit(int? id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (id == null)
                {
                    return NotFound();
                }

                var student = unitOfWork.Students.GetById(id);
                if (student == null)
                {
                    return NotFound();
                }
                ViewData["GroupName"] = new SelectList(unitOfWork.Groups.GetAll(), "Id", "Name", student.GroupId);
                return View(student);
            }
        }

        // POST: Student/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("FullName,DateBorn,Address,Gradebook_Number,GroupId,Id")] Student student)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (id != student.Id)
                {
                    return NotFound();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        unitOfWork.Students.Update(student);
                    }
                    catch (DbUpdateConcurrencyException ex)
                    {
                        throw ex;
                    }
                    return RedirectToAction(nameof(Index));
                }
                ViewData["GroupName"] = new SelectList(unitOfWork.Groups.GetAll(), "Id", "Name", student.GroupId);
                return View(student);
            }
        }

        // GET: Student/Delete/5
        public IActionResult Delete(int? id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (id == null)
                {
                    return NotFound();
                }

                var student = unitOfWork.Students.GetStudentsWithGroupsById(id);
                if (student == null)
                {
                    return NotFound();
                }

                return View(student);
            }
        }

        // POST: Student/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var student = unitOfWork.Students.GetById(id);
                unitOfWork.Students.Delete(student);
                return RedirectToAction(nameof(Index));
            }
        }
    }
}

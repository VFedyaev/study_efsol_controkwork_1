﻿using Microsoft.EntityFrameworkCore;
using UniversityProjectControlwork.Models;

namespace UniversityProjectControlwork
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public DbSet<Group> Groups { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            :base(options)
        {
        }
    }
}

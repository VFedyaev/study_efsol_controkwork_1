﻿using System.Collections.Generic;
using UniversityProjectControlwork.Models.Base;

namespace UniversityProjectControlwork.Models
{
    public class Group : EntityBase
    {
        public string Name { get; set; }

        public ICollection<Student> Students { get; set; }

        public Group()
        {
            Students = new List<Student>();
        }
    }
}

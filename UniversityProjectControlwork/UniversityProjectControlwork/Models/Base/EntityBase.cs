﻿using System;

namespace UniversityProjectControlwork.Models.Base
{
    public class EntityBase
    {
        public int Id { get; set; }
    }
}

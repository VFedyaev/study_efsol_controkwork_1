﻿using System;
using System.ComponentModel.DataAnnotations;
using UniversityProjectControlwork.Models.Base;

namespace UniversityProjectControlwork.Models
{
    public class Student : EntityBase
    {
        public string FullName { get; set; }
        //Ilya you said that we mustn't use required in models, but can we use for example Display format?
        //or we mustn't use DataAnnotations in models never? I will ask you that question on our lesson!
        [DisplayFormat(DataFormatString = "{0:d}")]
        [DataType(DataType.Date)]
        public DateTime DateBorn { get; set; }
        public string Address { get; set; }
        public string Gradebook_Number { get; set; }

        public int GroupId { get; set; }
        public Group Group { get; set; }
    }
}
